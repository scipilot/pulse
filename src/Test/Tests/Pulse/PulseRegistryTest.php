<?php
/**
 * @author: scipilot
 * @since: 6/12/2014
 */
namespace Scipilot\Pulse\Test\Tests\Pulse;

use Scipilot\Pulse\Config\JsonFileConfig;
use Scipilot\Pulse\App\Container;
use Scipilot\Pulse\Pulse\Pulse;
use Scipilot\Pulse\Pulse\PulseRegistry;
use Scipilot\Pulse\Test\Fixtures\FixtureRepository;
use Scipilot\Pulse\Test\Mocks\NullMockContainer;

class PulseRegistryTest extends \PHPUnit_Framework_TestCase {

	/**
	 * @var Container
	 */
	protected $app;

	/**
	 * @var PulseRegistry
	 */
	protected $pulseRegistry;

	public function setup(){
		$this->app = new NullMockContainer();
		// replace with the a proper config.
		$this->app->config = new JsonFileConfig($this->app);
		$this->pulseRegistry = new PulseRegistry($this->app);
	}

	public function testListPulses() {
		/** @var Pulse $p1 */
		/** @var Pulse $p2 */
		/** @var Pulse $p3 */

		$p1 = new Pulse($this->app, FixtureRepository::TEST_PULSE_ID1, FixtureRepository::TEST_PULSE_NAME1);
		$this->pulseRegistry->add($p1);
		$p2 = new Pulse($this->app, FixtureRepository::TEST_PULSE_ID2, FixtureRepository::TEST_PULSE_NAME2);
		$this->pulseRegistry->add($p2);
		$p3 = new Pulse($this->app, FixtureRepository::TEST_PULSE_ID3, FixtureRepository::TEST_PULSE_NAME3);
		$this->pulseRegistry->add($p3);

		// todo white box - inspect the file?

		$aP = $this->pulseRegistry->listPulses();

		$this->assertEquals($p1->id(), $aP[0]);
		$this->assertEquals($p2->id(), $aP[1]);
		$this->assertEquals($p3->id(), $aP[2]);
	}

	public function testAdd(){
		/** @var Pulse $p */
		/** @var Pulse $p2 */

		$p = new Pulse($this->app, FixtureRepository::TEST_PULSE_ID1, FixtureRepository::TEST_PULSE_NAME1);
		$this->pulseRegistry->add($p);

		// todo white box - inspect the file?

		$p2 = $this->pulseRegistry->get(FixtureRepository::TEST_PULSE_ID1);

		$this->assertEquals($p->id(), $p2->id());
		$this->assertEquals($p->name(), $p2->name());
	}

	// covered by testAdd currently
	public function testGet(){
	}

	public function testRemove(){
		/** @var Pulse $p */
		/** @var Pulse $p2 */

		$p = new Pulse($this->app, FixtureRepository::TEST_PULSE_ID1, FixtureRepository::TEST_PULSE_NAME1);
		$this->pulseRegistry->add($p);
		$this->pulseRegistry->remove(FixtureRepository::TEST_PULSE_ID1);

		// todo white box - inspect the file?
		$p2 = $this->pulseRegistry->get(FixtureRepository::TEST_PULSE_ID1);

		$this->assertNull($p2);
	}
}

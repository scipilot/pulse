<?php
/**
 * @author: scipilot
 * @since: 5/12/2014
 */

namespace Scipilot\Pulse\Test\Tests\Config;

use Scipilot\Pulse\Config\JsonFileConfig;

class JsonFileConfigTest extends FileConfigTest {

	public function setup(){
		parent::setup();
		$this->FIXTURE_FILENAME = 'config.json';
		$this->makeTestSubject();
	}

	protected function makeTestSubject(){
		$this->config = new JsonFileConfig($this->container);
	}

	// seems wrong to have to replicate this here....?
	protected function decode($enc) {
		return json_decode($enc);
	}

	protected function encode($dec) {
		return json_encode($dec);
	}
}

<?php
/**
 * @author: scipilot
 * @since: 5/12/2014
 */

namespace Scipilot\Pulse\Test\Tests\Config;

use Scipilot\Pulse\Config\SerialisedFileConfig;

class SerialisedFileConfigTest extends FileConfigTest {

	public function setup(){
		parent::setup();
		$this->FIXTURE_FILENAME = 'config.ser';
		$this->makeTestSubject();
	}

	protected function makeTestSubject(){
		$this->config = new SerialisedFileConfig($this->container);
	}

	protected function decode($enc) {
		return unserialize($enc);
	}

	protected function encode($dec) {
		return serialize($dec);
	}
}

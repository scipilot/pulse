<?php
/**
 * Simple JSON file implementation of the heartbeat database storage interface.
 *
 * @author: scipilot
 * @since: 5/12/2014
 */

namespace Scipilot\Pulse\Test\Tests\Storage;

use Scipilot\Pulse\App\Container;
use Scipilot\Pulse\Pulse\Beat;
use Scipilot\Pulse\Storage\IStorage;
use Scipilot\Pulse\Test\Mocks\NullMockContainer;

abstract class StorageTest extends \PHPUnit_Framework_TestCase {
	protected $FIXTURE_FILENAME = 'x';//overridden by daughter
	protected static $TMP_PATH;

	/**
	 * @var Container
	 */
	protected $app;
	/**
	 * @var IStorage
	 */
	protected $storage;

	public static function setUpBeforeClass(){
		self::$TMP_PATH = __DIR__.'/../../../../storage/';
	}

	public function setup(){
		$this->app = new NullMockContainer();
		$this->makeTestSubject();
	}

	// this should setup the System Under Test.
	protected abstract function makeTestSubject();

	// Covers both read and write of a dynamic timestamp
	public function testReadWrite(){
		$beat = new Beat(1);
		$this->storage->write($beat);

		$beat2 = $this->storage->read(1);

		$this->assertEquals($beat, $beat2);
	}

}

<?php
/**
 * PDO DB implementation of the heartbeat database storage interface.
 *
 * @author: scipilot
 * @since: 5/12/2014
 */

namespace Scipilot\Pulse\Test\Tests\Storage;

use Scipilot\Pulse\Config\JsonFileConfig;
use Scipilot\Pulse\Storage\PdoDbStorage;
use Scipilot\Pulse\Test\Fixtures\FixtureRepository;

class PdoDbStorageTest extends StorageTest {

	/**
	 * @var PdoDbStorage
	 */
	protected $storage;

	public function setup(){
		parent::setup();
		$this->FIXTURE_FILENAME = 'storage.ser';
		$this->app->config = new JsonFileConfig($this->app);
		//("storage.db.pdo.dsn": "sqlite:storage/pulse.sqlite"
		$this->markTestSkipped();
	}

	protected function makeTestSubject(){
		// TODO: $this->storage = new PdoDbStorage($this->app);
	}

	// white box tests the storage file
	public function testRead(){
		$this->markTestSkipped();

		$fixture = FixtureRepository::get($this->FIXTURE_FILENAME);
		$dbExpected = unserialize($fixture);

		// insert the fixture into the tmp folder
		file_put_contents(self::$TMP_PATH.$this->FIXTURE_FILENAME, $fixture);
		$beatActual = $this->storage->read(1);

		$this->assertEquals($dbExpected['_1'], $beatActual);
	}

	// white box tests the storage file
	public function testWrite(){
		$this->markTestSkipped();

		$fixture = FixtureRepository::get($this->FIXTURE_FILENAME);
		$dbExpected = unserialize($fixture);

		// write and fetch the stored result from the tmp folder
		$this->storage->write($dbExpected['_1']);
		$storageActual = file_get_contents(self::$TMP_PATH.$this->FIXTURE_FILENAME);

		$this->assertEquals($fixture, $storageActual);
	}

}

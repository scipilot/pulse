<?php
/**
 * Simple JSON file implementation of the heartbeat database storage interface.
 *
 * @author: scipilot
 * @since: 5/12/2014
 */

namespace Scipilot\Pulse\Test\Tests\Storage;

use Scipilot\Pulse\Pulse\Beat;
use Scipilot\Pulse\Storage\JsonFileStorage;
use Scipilot\Pulse\Test\Fixtures\FixtureRepository;

class JsonFileStorageTest extends StorageTest {

	/**
	 * @var JsonFileStorage
	 */
	protected $storage;

	public function setup(){
		parent::setup();
		$this->FIXTURE_FILENAME = 'storage.json';
	}

	protected function makeTestSubject(){
		$this->storage = new JsonFileStorage($this->app);
	}

	// white box tests the storage file
	public function testRead(){
		$jsonExpected = FixtureRepository::get($this->FIXTURE_FILENAME);
		$db = json_decode($jsonExpected);

		// insert the fixture into the tmp folder
		file_put_contents(self::$TMP_PATH.$this->FIXTURE_FILENAME, $jsonExpected);
		$beatActual = $this->storage->read(1);

		$this->assertEquals(new Beat($db->_1->iPulseId, new \DateTime($db->_1->dtTimestamp->date)), $beatActual);
	}

	// white box tests the storage file
	public function testWrite(){
		$jsonExpected = FixtureRepository::get($this->FIXTURE_FILENAME);
		$db = json_decode($jsonExpected);

		// write and fetch the stored result from the tmp folder
		$this->storage->write(new Beat($db->_1->iPulseId, new \DateTime($db->_1->dtTimestamp->date)));
		$jsonActual = file_get_contents(self::$TMP_PATH.$this->FIXTURE_FILENAME);

		$this->assertEquals($jsonExpected, $jsonActual);
	}

}

<?php
/**
 * @author: scipilot
 * @since: 7/12/2014
 */
namespace Scipilot\Pulse\Test\Tests\Scope;

use Scipilot\Pulse\App\Container;
use Scipilot\Pulse\Scope\IScope;
use Scipilot\Pulse\Scope\ScopeRegistry;
use Scipilot\Pulse\Test\Mocks\TestContainer;

abstract class ScopeTest extends \PHPUnit_Framework_TestCase {
	/**
	 * @var Container
	 */
	protected $app;

	/**
	 * @var IScope
	 */
	protected $scope;

	/**
	 * @var ScopeRegistry
	 */
	protected $registry;

	public static function setUpBeforeClass(){
	}

	public function setup(){
		$this->app = new TestContainer();
		$this->makeTestSubject();
	}

	// this should setup the System Under Test.
	protected abstract function makeTestSubject();


}
 
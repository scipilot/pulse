<?php
/**
 * This test naturally requires a database - so please create the schema and grant access as per the config.
 *
 * e.g.
 *
 * 	CREATE TABLE `system` (
 * 	  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
 * 	  `last_seen` datetime DEFAULT NULL,
 * 	  PRIMARY KEY (`id`)
 * 	) ENGINE=InnoDB;
 * 	INSERT INTO `system` (`id`, `last_seen`) VALUES ('1', NOW());
 *
 * 	CREATE USER 'pulse_test'@'localhost' IDENTIFIED BY 'pulse_test';
 * 	GRANT SELECT, UPDATE ON `pulse\_test`.* TO 'pulse_test'@'localhost';
 * 	FLUSH PRIVILEGES;

 * @author: scipilot
 * @since: 7/12/2014
 */
namespace Scipilot\Pulse\Test\Tests\Scope;

use Scipilot\Pulse\Pulse\Pulse;
use Scipilot\Pulse\Scope\ScopeRegistry;
use Scipilot\Pulse\Test\Fixtures\FixtureRepository;

class DatabaseRecordScopeTest extends ScopeTest {
	private $aConfPdo;

	public function setup(){
		parent::setup();

		$scopes = $this->app->config->get('scopes');
		$scope = $scopes->{'_'.FixtureRepository::TEST_SCOPE_DB_ID};// whitebox fragile!
		$this->aConfPdo = $scope->DatabaseRecordScope;// implementation-specific config
	}

	protected function makeTestSubject(){
		// use a registry - as it configures the Scope for us from the test-config.
		$this->registry = new ScopeRegistry($this->app);
		$this->scope = $this->registry->get(FixtureRepository::TEST_SCOPE_DB_ID);
	}
	
	public function testListen(){

		$pulse = new Pulse($this->app, FixtureRepository::TEST_PULSE_ID1, FixtureRepository::TEST_PULSE_NAME1);
		$dt = new \DateTime();
		$this->setDBTimestamp($dt);

		// run the test
		$this->scope->Listen($pulse);

		// check heartbeat was saved correctly
		$beat = $this->app->storage->read(FixtureRepository::TEST_PULSE_ID1);
		$this->assertEquals($dt, $beat->dtTimestamp);
	}

	// fixture function
	private function setDBTimestamp(\DateTime $dt){
		$dbh = new \PDO($this->aConfPdo->dsn, $this->aConfPdo->username, $this->aConfPdo->password);

		$st = $dbh->prepare($this->aConfPdo->update);
		$st->execute(array($dt->format('Y-m-d H:i:s')));

	}
}

<?php
/**
 * @author: scipilot
 * @since: 6/12/2014
 */
namespace Scipilot\Pulse\Test\Tests\Monitor;

use Scipilot\Pulse\App\Container;
use Scipilot\Pulse\Log\ILog;
use Scipilot\Pulse\Monitor\Monitor;
use Scipilot\Pulse\Notify\EmailNotify;
use Scipilot\Pulse\Pulse\Pulse;
use Scipilot\Pulse\Pulse\PulseRegistry;
use Scipilot\Pulse\Pulse\Status;
use Scipilot\Pulse\Test\Mocks\TestContainer;

class MonitorTest extends \PHPUnit_Framework_TestCase {
	const TEST_PULSE_ID1 = 1;
	const TEST_PULSE_NAME1 = 'Monitor Scan Unit Test Service #1';

	/**
	 * @var Container
	 */
	protected $app;

	/**
	 * @var PulseRegistry
	 */
	protected $pulseRegistry;

	/**
	 * @var Monitor
	 */
	protected $monitor;

	public function setup(){
		// we need the whole app for this test
		$this->app = new TestContainer();
		$this->app->log->setVerbosity(ILog::LOG_LEVEL_DEBUG);

		$this->pulseRegistry = new PulseRegistry($this->app);
		$this->monitor = new Monitor($this->app);

		// Setup the config
		// todo move to config-UNITTEST.json now we have it
		$this->app->config->set(EmailNotify::CONFIG_ROOT.'.default.email.to','', true);
		$this->app->config->set(EmailNotify::CONFIG_ROOT.'.default.email.subject', self::TEST_PULSE_NAME1, true);
		$this->app->config->set(EmailNotify::CONFIG_ROOT.'.default.email.body','type:_TYPE_ level:_LEVEL_ message:_MESSAGE_', true);
	}

	/**
	 * Checks the scanScopes function correctly inserts a Beat via a Scope.
	 */
	public function testScanScopes(){
		// get the timestamp out of the fixture config (the rest is also used by the system)
		$conf = $this->app->config->get('scopes');
		$scope = $conf->_1;// whitebox fragility!
		$time = $scope->MockScope->timestamp;

		$this->monitor->testScanScopes();

		// check the beat is there
		$beat = $this->app->storage->read($scope->pulseId);

		$this->assertEquals(new \DateTime($time), $beat->dtTimestamp);
	}

	/**
	 * This tests the monitoring system can detect and notify an ALERT state.
	 * It sets up a services and makes it fail a bit.
	 */
	public function testScanAlert(){
		/** @var Pulse $p1 */
		/** @var Pulse $p2 */
		/** @var Pulse $p3 */

		// pulse 1 is set a 1 second alert, 2 second alarm
		$p1 = new Pulse($this->app, self::TEST_PULSE_ID1, self::TEST_PULSE_NAME1);

		$p1->setConfig((object)array('alertTimeout'=>1, 'alarmTimeout'=>3));
		$this->pulseRegistry->add($p1);

		//		$this->app->config->set('monitor.run.interval', 1, true);
		//		$this->app->config->set('monitor.run.lifespan', 0, true);

		// initial beat.
		$p1->beat();
		$this->monitor->scan();
		$p1 = $this->pulseRegistry->get($p1->id());// this is annoying we have to refresh the persistence!
		$this->assertEquals(Status::OK, $p1->status);

		usleep(2000000);
		$this->monitor->scan();
		$p1 = $this->pulseRegistry->get($p1->id());// this is annoying!
		$this->assertEquals(Status::ALERT, $p1->status);

		usleep(2000000);
		$this->monitor->scan();
		$p1 = $this->pulseRegistry->get($p1->id());// this is annoying!
		$this->assertEquals(Status::ALARM, $p1->status);

		// resumption beat (these songs of freedom)
		$p1->beat();
		$this->monitor->scan();
		$p1 = $this->pulseRegistry->get($p1->id());// this is annoying!
		$this->assertEquals(Status::OK, $p1->status);

	}
}
 
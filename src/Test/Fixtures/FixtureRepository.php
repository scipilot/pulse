<?php
/**
 * Fixture repository fetches the contents of fixture files for you.
 *
 * @author: scipilot
 * @since: 5/12/2014
 */

namespace Scipilot\Pulse\Test\Fixtures;


class FixtureRepository {
	const TEST_SCOPE_MOCK_ID = 1; // Mock Scope  in config
	const TEST_SCOPE_DB_ID = 2; 	// DB Scope in config

	const TEST_PULSE_ID1 = 1;
	const TEST_PULSE_NAME1 = 'Unit Test Service #1';
	const TEST_PULSE_ID2 = 2;
	const TEST_PULSE_NAME2 = 'Unit Test Service #2';
	const TEST_PULSE_ID3 = 3;
	const TEST_PULSE_NAME3 = 'Unit Test Service #3';

	public static function get($filename){
		return file_get_contents(__DIR__.'/'.$filename);
	}
} 
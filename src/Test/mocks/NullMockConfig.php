<?php
/**
 * @author: scipilot
 * @since: 5/12/2014
 */

namespace Scipilot\Pulse\Test\Mocks;

use Scipilot\Pulse\Config\IConfig;

class NullMockConfig implements IConfig {

	public function add($sKey, $sSubKey, $mValue, $bPersist = false) {
	}

	public function get($sKey, $bRefresh = false) {
	}

	public function set($sKey, $mValue, $bPersist=false) {
	}


}

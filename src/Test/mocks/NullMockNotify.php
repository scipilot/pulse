<?php
/**
 * @author: scipilot
 * @since: 5/12/2014
 */

namespace Scipilot\Pulse\Test\Mocks;

use Scipilot\Pulse\Notify\INotify;

class NullMockNotify implements INotify {

	public function send($iType, $iLevel, $iPulseId, $sMessage = '') {
	}

}

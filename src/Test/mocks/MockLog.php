<?php
/**
 * Echo's the logs.
 *
 * @author: scipilot
 * @since: 5/12/2014
 */

namespace Scipilot\Pulse\Test\Mocks;

use Scipilot\Pulse\Log\ILog;

class MockLog implements ILog {

	public function setVerbosity($iVerbosity) {
	}


	public function write($sMessage, $iVerbosity=self::LOG_LEVEL_INFO) {
		echo __METHOD__.$sMessage;
	}

}

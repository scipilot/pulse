<?php
/**
 * Ze storage, it does nothing!
 *
 * @author: scipilot
 * @since : 5/12/2014
 */

namespace Scipilot\Pulse\Test\Mocks;

use Scipilot\Pulse\Pulse\Beat;
use Scipilot\Pulse\Storage\IStorage;

class NullMockStorage implements IStorage {

	public function write(Beat $beat) {
		// do nothing
	}

	public function read($id) {
		return new Beat(0);
	}

}

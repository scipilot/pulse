<?php
/**
 * Just echo's.
 *
 * @author: scipilot
 * @since: 5/12/2014
 */

namespace Scipilot\Pulse\Test\Mocks;

use Scipilot\Pulse\Notify\INotify;

class MockNotify implements INotify {

	public function send($iType, $iLevel, $iPulseId, $sMessage = '') {
		echo "send($iType, $iLevel, $iPulseId, $sMessage)";
	}

}

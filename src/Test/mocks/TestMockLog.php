<?php
/**
 * Logs to the Tests/log folder.
 *
 * @author: scipilot
 * @since: 5/12/2014
 */
namespace Scipilot\Pulse\Test\Mocks;

use Scipilot\Pulse\Log\LocalLog;

class TestMockLog extends LocalLog {

	function __construct() {
		self::$PATH_s = __DIR__.'/../log/pulse-%s.log';
	}
}

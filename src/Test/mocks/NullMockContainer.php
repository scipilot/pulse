<?php
/**
 * Mock container - contains component mocks which don't do anything!
 *
 * @author: scipilot
 * @since: 5/12/2014
 */

namespace Scipilot\Pulse\Test\Mocks;

use Scipilot\Pulse\App\Container;

class NullMockContainer extends Container {

	function __construct(){
		$this->config = new NullMockConfig($this);
		$this->log = new NullMockLog($this);
		$this->notify = new NullMockNotify($this);
		$this->storage = new NullMockStorage($this);
	}
}

<?php
/**
 * @author: scipilot
 * @since: 5/12/2014
 */

namespace Scipilot\Pulse\Test\Mocks;

use Scipilot\Pulse\App\Container;

class TestContainer extends Container {

	function __construct(){
		$this->config = new TestMockJsonFileConfig($this);
		$this->log = new TestMockLog($this);
		$this->notify = new TestMockNotify($this);
		$this->storage = new TestMockSerialisedFileStorage($this);
	}
}

<?php
/**
 * Provides config for the Unit Tests, from config-UNITTEST.json.
 *
 * Avoids corrupting the real configuration.
 *
 * @author: scipilot
 * @since: 5/12/2014
 */
namespace Scipilot\Pulse\Test\Mocks;

use Scipilot\Pulse\App\Container;
use Scipilot\Pulse\Config\JsonFileConfig;

class TestMockJsonFileConfig extends JsonFileConfig {

	function __construct(Container $appContainer) {
		parent::__construct($appContainer);

		$this->path = __DIR__.'/../../../storage/config-UNITTEST.json';
	}
}

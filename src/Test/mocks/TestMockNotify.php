<?php
/**
 * Logs notifications to the injected logs.
 * (Assuming you have the TestMockLog in the app container, this will the Tests/log folder)
 *
 * @author: scipilot
 * @since: 5/12/2014
 */
namespace Scipilot\Pulse\Test\Mocks;

use Scipilot\Pulse\Notify\Notify;

class TestMockNotify extends Notify {

	public function send($iType, $iLevel, $iPulseId, $sMessage = '') {
		$this->app->log->write('MOCK NOTIFY: send($iType, $iLevel, $iPulseId, $sMessage)');
	}
}

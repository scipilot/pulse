<?php
/**
 * Mock container - contains component mocks which don't do anything!
 *
 * @author: scipilot
 * @since: 5/12/2014
 */

namespace Scipilot\Pulse\Test\Mocks;

use Scipilot\Pulse\App\Container;

class MockContainer extends Container {

	function __construct(){
		$this->log = new MockLog();
		$this->config = new MockConfig();
		$this->notify = new MockNotify();
		$this->storage = new MockStorage();
	}
}

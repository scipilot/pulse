<?php
/**
 * This is an example usage, and complete system test.
 *
 * @author: scipilot
 * @since: 6/12/2014
 */
require_once (__DIR__.'/../../vendor/autoload.php');

use	Scipilot\Pulse\Notify\INotify;
use Scipilot\Pulse\Pulse\Beat;
use Scipilot\Pulse\Pulse\Pulse;
use Scipilot\Pulse\App\DefaultContainer;
use Scipilot\Pulse\Notify\EmailNotify;

$pID = 'pulseSystemTest';
$app = new DefaultContainer();
$pulse = new Pulse($app, $pID,'Pulse System Test');

// Setup the config
$app->config->set(EmailNotify::CONFIG_ROOT.'.default.email.to','', true);
$app->config->set(EmailNotify::CONFIG_ROOT.'.default.email.subject',$pulse->name(), true);
$app->config->set(EmailNotify::CONFIG_ROOT.'.default.email.body','type:_TYPE_ level:_LEVEL_ message:_MESSAGE_', true);

// self test the system test.
$emailCheck = $app->config->get(EmailNotify::CONFIG_ROOT.'.default.email.to');
if(empty($emailCheck)){
	trigger_error('You need to configure the default.email.to before running the system test.');
	exit;
}

// Send a welcome Notify
$app->notify->send(INotify::TYPE_MONITOR, INotify::MONITOR_STATUS, $pID, 'System Test Starting...');

// Start the 'service'
$iCount = 0;
$iLimit = 3;
while($iCount++ < $iLimit){

	sleep(1);

	$app->storage->write(new Beat($pID));
	$app->notify->send(INotify::TYPE_MONITOR, INotify::MONITOR_STATUS, $pID, 'Hearbeat '.$iCount);
}

// 'die'
$app->notify->send(INotify::TYPE_MONITOR, INotify::MONITOR_ALERT, $pID, 'System Test Alert!');
$app->notify->send(INotify::TYPE_MONITOR, INotify::MONITOR_ALARM, $pID, 'System Test Alarm!');
$app->notify->send(INotify::TYPE_INTERNAL, INotify::INTERNAL_DEBUG, $pID, 'System Test Debug');
$app->notify->send(INotify::TYPE_INTERNAL, INotify::INTERNAL_WARNING, $pID, 'System Test Warning');
$app->notify->send(INotify::TYPE_INTERNAL, INotify::INTERNAL_ERROR, $pID, 'System Test Error');
$app->notify->send(INotify::TYPE_INTERNAL, INotify::INTERNAL_FATAL, $pID, 'System Test Fatal');

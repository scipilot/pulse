<?php
/**
 * @author: scipilot
 * @since: 5/12/2014
 */
namespace Scipilot\Pulse\Scope;

use Scipilot\Pulse\Scope\IScope;
use Scipilot\Pulse\App\Contained;
use Scipilot\Pulse\App\Container;

class ScopeRegistry extends Contained {

	function __construct(Container $appContainer){
		parent::__construct($appContainer);
	}

	/**
	 * Gets an array of all active Scope Ids.
	 */
	public function listScopes(){
		$a = array();
		$b = array();

		$configs = $this->app->config->get('scopes');
		if(!empty($configs)) $a = array_keys((array)$configs);

		// strip off the _ prefixes (which prevent index collision in php's weird dual-purpose arrays).
		foreach($a as $pid){
			$b[] = substr($pid, 1);
		}

		return $b;
	}

	/**
	 * Registers a Scope in the config, to be monitored.
	 *
	 * @param IScope $scope
	 */
	public function add(IScope $scope){
		$config = $scope->getConfig();

		$this->app->config->add('scopes', '_'.$scope->id(), $config, true);
	}
	/**
	 * Instantiates a Scope from its configuration.
	 *
	 * @param int $iScopeId
	 * @return IScope|null
	 */
	public function get($iScopeId){
		/** @var IScope $scope */
		$scope = null;

		$conf = $this->app->config->get('scopes');
		if($conf){
			$key = '_'.$iScopeId;
			$scopeConf = $conf->$key;
			if($scopeConf){
				// detect the strategy
				$class = 'Scipilot\\Pulse\\Scope\\'.$scopeConf->class;
				$scope = new $class($this->app, $scopeConf->id);
				$scope->setConfig($scopeConf);
			}
		}
		return $scope;
	}

	/**
	 * Removes a Scope from the registry - to stop monitoring this service.
	 *
	 * @param int $iScopeID
	 */
	public function remove($iScopeID){
		$this->app->config->add('Scopes', '_'.$iScopeID, null, true); //write an unset?
	}
}

<?php
/**
 * A Scope helps the Monitor read Beats itself, from a remote source e.g. top/ps, PID file, DB or API.
 *
 * This is a non-invasive alternative to the Pulse-beat() being included in your own application.
 *
 * @author: scipilot
 * @since : 7/12/2014
 */

namespace Scipilot\Pulse\Scope;

use Scipilot\Pulse\App\Container;
use Scipilot\Pulse\Pulse\Pulse;

interface IScope {

	function __construct(Container $appContainer, $id);

	/**
	 * Gets a config-object representation of this class.
	 * @return \stdClass (object)array('id'=>int, 'pulseId'=>int, ...)
	 */
	public function getConfig();

	/**
	 * Re-sets up this class from a config-object representation
	 * See specific class documentation for config specifics.
	 *
	 * @param $oConfig \stdClass (object)array('pulseId'=>int, ...)
	 */
	public function setConfig($oConfig);

	/**
	 * Checks a Pulse using the Scope's particular method, recording a Beat in the Storage.
	 *
	 * @param Pulse|null $pulse If empty we use the one from our config (usual).
	 *
	 * @return bool success
	 */
	public function Listen(Pulse $pulse = null);
}

<?php
/**
 * Reads a heartbeat from a single database record.
 *
 * Alls Scopes require configuration.
 * This implementation (strategy) stores its config in a subkey:
 *
 * (The scopes are indexed by their ID which requires a string in JSON and a prefix in PHP, so we use _.)
 *
 * scopes._ID.DatabaseRecordScope:
 *
 *  dsn:          DSN for any supported drivers on your system.
 *  username:     (user/pass settings are not needed in some DSNs)
 *  password:
 *  //database:   Included in all DSNs (so far)
 *  select:       SQL query to select the record. 				E.G. "SELECT UNIX_TIMESTAMP(lastSeen) FROM tblStatus WHERE appId = 1"
 *  update:       SQL query to UPDATE select the record. 	E.G. "UPDATE tblStatus SET lastSeen=? WHERE id=1"
 *  field_name:   column in the results e.g. "lastSeen"
 *  field_type:   "TIMESTAMP" | "DATETIME" | "ISO8601"
 *
 * @author: scipilot
 * @since : 7/12/2014
 */
namespace Scipilot\Pulse\Scope;

use Scipilot\Pulse\Log\ILog;
use Scipilot\Pulse\Notify\INotify;
use Scipilot\Pulse\Pulse\Beat;
use Scipilot\Pulse\Pulse\Pulse;

class DatabaseRecordScope extends Scope {

	/**
	 * @var array
	 */
	protected $dbConf;

	/**
	 * See class documentation for pdo config node's children.
	 *
	 * @return \stdClass (object)array('id'=>int, 'pdo'=>array('dsn', ... ))
	 */
	public function getConfig() {
		$oConfig = parent::getConfig();
		$oConfig->pdo = $this->dbConf;
		return $oConfig;
	}

	/**
	 * See class documentation for pdo config node's children.
	 *
	 * @param $oConfig \stdClass (object)array('DatabaseRecordScope'=>array('dsn', ... ))
	 */
	public function setConfig($oConfig) {
		parent::setConfig($oConfig);
		$this->dbConf      = $oConfig->DatabaseRecordScope;
	}

	public function Listen(Pulse $pulse=null) {
		/** @var Beat $beat */
		$beat = null;
		$dt = null;

		if($pulse == null){
			$pulse = new Pulse($this->app, $this->iPulseId, 'pulse name todo!'/*todo: move inside pulse?*/);
		}

		// Connect to DB
		$pdo = new \PDO($this->dbConf->dsn, $this->dbConf->username, $this->dbConf->password);

		// Select from the specified table, row, column
		$st  = $pdo->query($this->dbConf->select);
		if($st){
			$row = $st->fetch();
			// check it worked
			if (isset($row[$this->dbConf->field_name])) {

				// convert timestamp
				switch ($this->dbConf->field_type){
					case 'TIMESTAMP':
						$dt = new \DateTime();
						$dt->setTimestamp($row[$this->dbConf->field_name]);
						break;
					case 'DATETIME':
					case 'ISO8601':
						$dt = new \DateTime($row[$this->dbConf->field_name]);
						break;
					default:
						$this->handleError(sprintf('%s ERROR interpreting beat for Scope: %d. Config has unknown pdo.field_type: %s',
							__METHOD__, $this->id, $st[$this->dbConf->field_type]));
				}

				// Create and store a Beat
				$beat = new Beat($pulse->id(), $dt);
				$this->app->storage->write($beat);
			}
			else {
				$this->handleError(sprintf('%s ERROR fetching beat for Scope: %d. PDO said: %s', __METHOD__, $this->id, var_export($pdo->errorInfo(), true)));
			}
		}
		else {
			$this->handleError(sprintf('%s ERROR fetching beat for Scope: %d. PDO said: %s', __METHOD__, $this->id, var_export($pdo->errorInfo(), true)));
		}

		return $beat != null;
	}

	private function handleError($sDebug) {
		$this->app->log->write($sDebug, ILog::LOG_LEVEL_ERROR);
		$this->app->notify->send(INotify::TYPE_INTERNAL, INotify::INTERNAL_ERROR, $sDebug);
	}
}

<?php
/**
 * Mock Scope, expects a fixed timestamp in the config, which it will put in every beat.
 *
 * @author: scipilot
 * @since: 7/12/2014
 */
namespace Scipilot\Pulse\Scope;

use Scipilot\Pulse\Pulse\Beat;
use Scipilot\Pulse\Pulse\Pulse;

class MockScope extends Scope {
	/**
	 * DateTime parse-able.
	 * @var string
	 */
	private $mockTimestamp;

	/**
	 * Specific config to MockScope.
	 *
	 * @param $oConfig \stdClass (object)array('MockScope'=>array('timestamp'))
	 */
	public function setConfig($oConfig) {
		parent::setConfig($oConfig);

		$this->mockTimestamp = $oConfig->MockScope->timestamp;
	}

	// todo: see the common code with the others: refactor: pull-up to parent?
	public function Listen(Pulse $pulse = null) {
		/** @var Beat $beat */
		$beat = null;
		$dt = new \DateTime($this->mockTimestamp);

		if($pulse == null){
			$pulse = new Pulse($this->app, $this->iPulseId, 'todo!'/*todo: move inside pulse?*/);
		}

		// Create and store a Beat
		$beat = new Beat($pulse->id(), $dt);
		$this->app->storage->write($beat);
	}
}

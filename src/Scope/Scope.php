<?php
/**
 * Base class for all Scope implementations (strategies).
 *
 * @author: scipilot
 * @since: 7/12/2014
 */
namespace Scipilot\Pulse\Scope;

use Scipilot\Pulse\App\Contained;
use Scipilot\Pulse\App\Container;
use Scipilot\Pulse\Pulse\Pulse;

abstract class Scope extends Contained implements IScope {
	/**
	 * @var int
	 */
	protected $id;

	/**
	 * @var int
	 */
	protected $iPulseId;

	function __construct(Container $appContainer, $id){
		parent::__construct($appContainer);

		$this->id = $id;
	}

	function id(){
		return $this->id;
	}

	public function getConfig(){
		$oConfig = new \stdClass();
		$oConfig->id = $this->id;
		$oConfig->pulseId = $this->iPulseId;
		return $oConfig;
	}

	public function setConfig($oConfig){
		if(!empty($oConfig->pulseId)) $this->iPulseId = $oConfig->pulseId;
	}

	/**
	 * @return int
	 */
	public function getPulseId(){
		return $this->iPulseId;
	}
}

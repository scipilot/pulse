<?php
/**
 * Simple base class for all items contained in the container providing circular reference.
 *
 * @author: scipilot
 * @since: 6/12/2014
 */
namespace Scipilot\Pulse\App;

class Contained {

	/**
	 * @var Container
	 */
	protected $app;

	/**
	 * @param Container $appContainer 	IoC
	 */
	function __construct(Container $appContainer){
		$this->app = $appContainer;
	}
}

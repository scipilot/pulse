<?php
/**
 * Default set of components: Json config, local log, serialised storage, email notifications.
 * Use this if you don't want to have to inject your own component dependencies.
 *
 * @author: scipilot
 * @since: 6/12/2014
 */
namespace Scipilot\Pulse\App;

use Scipilot\Pulse\Config\JsonFileConfig;
use Scipilot\Pulse\Log\LocalLog;
use Scipilot\Pulse\Notify\EmailNotify;
use Scipilot\Pulse\Storage\SerialisedFileStorage;

class DefaultContainer extends Container {

	function __construct(){
		// bit worried about timing here: container isn't fully populated... do config first, then logs.
		$this->config = new JsonFileConfig($this);
		$this->log = new LocalLog();
		$this->storage = new SerialisedFileStorage($this);
		$this->notify = new EmailNotify($this);
	}

}

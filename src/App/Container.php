<?php
/**
 * The base IoC container. Use this if you want to inject your own set of components, else see DefaultContainer.
 *
 * @author: scipilot
 * @since: 5/12/2014
 */
namespace Scipilot\Pulse\App;

use Scipilot\Pulse\Config\IConfig;
use Scipilot\Pulse\Log\ILog;
use Scipilot\Pulse\Notify\INotify;
use Scipilot\Pulse\Storage\IStorage;

class Container {

	/**
	 * @var ILog
	 */
	public $log;

	/**
	 * @var INotify
	 */
	public $notify;

	/**
	 * @var IStorage
	 */
	public $storage;

	/**
	 * @var IConfig
	 */
	public $config;

} 
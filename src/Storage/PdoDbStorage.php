<?php
/**
 *
 *
 *
 *
 *
 * @todo not finished, nor tested!
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 * PDO DB implementation of the heartbeat database storage interface.
 *
 * Requires configuration (user/pass settings not needed in some DSNs):
 *
 *  storage.db.pdo.dsn:
 *  storage.db.pdo.username:
 *  storage.db.pdo.password:
 *  storage.db.pdo.database:        default 'pulse' - also included in all DSNs
 *  storage.db.pdo.table:            default 'beat'
 *  storage.db.pdo.field_pulse_id:  default 'pulse_id'
 *  storage.db.pdo.field_heartbeat: default 'beat_time' - Unix timestamp
 *
 * DSN examples:
 *  MySQL Driver format DSN:  'mysql:host=127.0.0.1;dbname=pulse;charset=utf8'
 *  Windows System DSN:        'odbc:DSN=pulse_dsn;Uid=pulse_user;Pwd=pulse_password'
 *  ODBC-MySQL format DSN:    'odbc:Server=127.0.0.1;Database=pulse;Uid=pulse_user;Pwd=pulse_password;'
 *  SQLite:                  'sqlite:/opt/databases/mydb.sq3'
 *
 * Stores Pulse heartbeats in simple table structure:
 *
 * e.g. SQLite:
 *
 *   CREATE TABLE `beat` (
 *    `pulse_id`  INTEGER,
 *    `beat_time`  NUMERIC,
 *    PRIMARY KEY(pulse_id)
 *  );
 *
 * @author: scipilot
 * @since : 5/12/2014
 */

namespace Scipilot\Pulse\Storage;

use Scipilot\Pulse\App\Container;
use Scipilot\Pulse\Log\ILog;
use Scipilot\Pulse\Notify\INotify;
use Scipilot\Pulse\Pulse\Beat;

class PdoDbStorage extends Storage {

	/**
	 * @var \PDO
	 */
	protected $pdo;

	protected $username;
	protected $password;
	protected $database = 'pulse';
	protected $table = 'beat';
	protected $field_pulse_id = 'pulse_id';
	protected $field_heartbeat = 'heartbeat';

	function __construct(Container $appContainer) {
		parent::__construct($appContainer);

		// Process optional config and defaults.
		//		$this->app->config->get('storage.db.pdo.dsn');
		$username        = $this->app->config->get('storage.db.pdo.username');
		$password        = $this->app->config->get('storage.db.pdo.password');
		$database        = $this->app->config->get('storage.db.pdo.database');
		$table           = $this->app->config->get('storage.db.pdo.table');
		$field_pulse_id  = $this->app->config->get('storage.db.pdo.field_pulse_id');
		$field_heartbeat = $this->app->config->get('storage.db.pdo.field_heartbeat');
		if (!empty($username)) $this->username = $username;
		if (!empty($password)) $this->password = $password;
		if (!empty($database)) $this->database = $database;
		if (!empty($table)) $this->table = $table;
		if (!empty($field_pulse_id)) $this->field_pulse_id = $field_pulse_id;
		if (!empty($field_heartbeat)) $this->field_heartbeat = $field_heartbeat;
	}

	private function load() {
		if (empty($this->pdo)) {
			$confDSN   = $this->app->config->get('storage.db.pdo.dsn');
			$this->pdo = new \PDO($confDSN, $this->username, $this->password);
		}
	}

	private function persist($db) {
		// this driver always persists anyway!
		return true;
	}

	public function write(Beat $beat) {
		$bResult = true;
		$this->load();

		$sql = 'REPLACE INTO ' . $this->table . ' (' . $this->field_pulse_id . ',' . $this->field_heartbeat . ') VALUES (?,?)';
		$st  = $this->pdo->prepare($sql);
		if($st) {
			if (!$st->execute(array($beat->iPulseId, $beat->dtTimestamp))) {
				$bResult = false;
			}
		}
		else {
			$bResult = false;
		}

		if(!$bResult){
			$this->handleError(sprintf('%s ERROR storing beat for Pulse: $d. PDO said: %s', __METHOD__, $beat->iPulseId, $this->pdo->errorCode()));
		}
	}

	public function read($id) {
		/** @var Beat $beat */
		$beat = null;

		$this->load();

		$sql = 'SELECT ' . $this->field_pulse_id . ' iPulseId, ' . $this->field_heartbeat . ' dtTimestamp '
			. ' FROM ' . $this->table
			. ' WHERE ' . $this->field_pulse_id . '=?';
		$st  = $this->pdo->prepare($sql);
		if ($st->execute(array($beat->iPulseId))) {
			$beat = $st->fetchObject('Scipilot\Pulse\Pulse\Beat');
		}
		else {
			$this->handleError(sprintf('%s ERROR reading beat for Pulse: $d. PDO said: %s', __METHOD__, $beat->iPulseId, $this->pdo->errorInfo()));
		}

		return $beat;
	}

	private function handleError($sDebug) {
		$this->app->log->write($sDebug, ILog::LOG_LEVEL_ERROR);
		$this->app->notify->send(INotify::TYPE_INTERNAL, INotify::INTERNAL_ERROR, $sDebug);
	}
}

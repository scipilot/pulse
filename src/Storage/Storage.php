<?php
/**
 * @author: scipilot
 * @since: 6/12/2014
 */

namespace Scipilot\Pulse\Storage;

use Scipilot\Pulse\App\Container;

abstract class Storage implements IStorage {

	/**
	 * @var Container
	 */
	protected $app;

	/**
	 * @param Container $appContainer 	IoC
	 */
	function __construct(Container $appContainer){
		$this->app = $appContainer;
		$this->path = __DIR__.'/../../storage/storage.json';
	}
}

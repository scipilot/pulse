<?php
/**
 * @author: scipilot
 * @since: 5/12/2014
 */
namespace Scipilot\Pulse\Storage;

use Scipilot\Pulse\Pulse\Beat;

interface IStorage {

	/**
	 * Updates the heartbeat for this service to NOW.
	 *
	 * @param Beat $beat
	 * @return mixed
	 */
	public function write(Beat $beat);

	/**
	 * Gets the latest heartbeat for a service.
	 *
	 * @param int $id	Unique Pulse ID for a Service/Application.
	 * @return Beat
	 */
	public function read($id);
}
<?php
/**
 * Simple JSON file implementation of the heartbeat database storage interface.
 *
 * @author: scipilot
 * @since: 5/12/2014
 */

namespace Scipilot\Pulse\Storage;

use Scipilot\Pulse\App\Container;
use Scipilot\Pulse\Pulse\Beat;

class JsonFileStorage extends Storage {

	function __construct(Container $appContainer){
		parent::__construct($appContainer);

		$this->path = __DIR__.'/../../storage/storage.json';
	}

	private function load(){
		if(!file_exists($this->path)){
			// todo: notify this?
			$this->app->log->write(__METHOD__.' WARNING: empty storage - initialising...');
			return new \stdClass();
		}
		return json_decode(file_get_contents($this->path));
	}

	private function persist($db){
		return file_put_contents($this->path, json_encode($db), LOCK_EX);
	}

	public function write(Beat $beat){
		$db = $this->load();
		// todo: race hazard here! lock the file.
		$idx = '_'.$beat->iPulseId;
		$db->$idx = $beat;
		$this->persist($db);
	}

	public function read($id){
		/** @var Beat $beat */
		$beat = null;

		$db = $this->load();
		$idx = '_'.$id;
		if(!empty($db->$idx)){
			$node = $db->$idx;
			$beat = new Beat($node->iPulseId, new \DateTime($node->dtTimestamp->date));
		}

		return $beat;
	}
}
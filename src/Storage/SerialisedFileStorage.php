<?php
/**
 * Serialised file implementation of the heartbeat database storage interface.
 *
 * @author: scipilot
 * @since: 5/12/2014
 */

namespace Scipilot\Pulse\Storage;

use Scipilot\Pulse\App\Container;
use Scipilot\Pulse\Pulse\Beat;

class SerialisedFileStorage extends Storage {

	function __construct(Container $appContainer){
		parent::__construct($appContainer);

		$this->path = __DIR__.'/../../storage/storage.ser';
	}

	private function load(){
		if(!file_exists($this->path)){
			// todo: notify this?
			$this->app->log->write(__METHOD__.' WARNING: empty storage - initialising...');
			return array();
		}
		return unserialize(file_get_contents($this->path));
	}

	private function persist($db){
		return file_put_contents($this->path, serialize($db), LOCK_EX);
	}

	public function write(Beat $beat){
		$db = $this->load();
		// todo: race hazard here! lock the file.
		$db['_'.$beat->iPulseId] = $beat;
		$this->persist($db);
	}

	public function read($id){
		/** @var Beat $beat */
		$beat = null;

		$db = $this->load();
		if(!empty($db['_'.$id])) $beat = $db['_'.$id];

		return $beat;
	}
}
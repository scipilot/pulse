<?php
/**
 * Serialised file implementation of the configuration interface.
 *
 * @author: scipilot
 * @since: 5/12/2014
 */

namespace Scipilot\Pulse\Config;

use Scipilot\Pulse\App\Container;

abstract class FileConfig implements IConfig {

	/**
	 * @var Container
	 */
	protected $app;

	/**
	 * @var array
	 */
	protected $db;

	/**
	 * Should be set in daughter constructor. e.g. __DIR__.'/../../storage/config.json';
	 * @var string
	 */
	protected $path;

	/**
	 * @param Container $appContainer 	IoC
	 */
	function __construct(Container $appContainer){
		$this->app = $appContainer;
		$this->path = __DIR__.'/../../storage/config.json';
	}

	/**
	 * Implemented by specific daughter.
	 * @param string $enc Encoded data.
	 */
	abstract protected function decode($enc);

	/**
	 * Implemented by specific daughter.
	 * @param string $dec Decoded data.
	 */
	abstract protected function encode($dec);

	private function load(){
		if(!file_exists($this->path)){
			// todo: notify this?
			$this->app->log->write(__METHOD__.'Warning: empty storage - initialising...');
			$this->db = new \stdClass();
			return;
		}
		$this->db = $this->decode(file_get_contents($this->path));
	}

	private function persist(){
		return file_put_contents($this->path, $this->encode($this->db), LOCK_EX);
	}

	// todo test it works in all storage engines
	public function add($sKey, $sSubKey, $mValue, $bPersist = false){
		if(empty($this->db)) $this->load();

		if(!empty($this->db->$sKey)){
			$arr = $this->db->$sKey;
		}
		else $arr = empty($sSubKey) ? array() : new \stdClass();

		// swap it over, couldn't seem to do it one go with a dynamic object property (key) name. use a reference?
		if(!empty($sSubKey)){
			$arr->$sSubKey = $mValue;
		}
		else {
			$arr[] = $mValue;
		}
		$this->db->$sKey = $arr;

		if($bPersist) $this->persist($this->db);
	}

	public function get($sKey, $bRefresh=false) {
		$conf = null;

		if(empty($this->db) || $bRefresh) $this->load();

		if(!empty($this->db->$sKey)) $conf = $this->db->$sKey;

		return $conf;
	}

	public function set($sKey, $mValue, $bPersist = false) {
		if(empty($this->db)) $this->load();

		$this->db->$sKey = $mValue;

		if($bPersist) $this->persist($this->db);
	}

}

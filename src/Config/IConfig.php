<?php
/**
 * @author: scipilot
 * @since : 5/12/2014
 */
namespace Scipilot\Pulse\Config;

interface IConfig {

	/**
	 * Similar to set() but it adds the value to an array at key.
	 *
	 * @param string $sKey     Config key name.
	 * @param string $sSubKey  Array index key, or leave empty if you want to use a numerically indexed array.
	 * @param mixed  $mValue
	 * @param bool   $bPersist Whether to write to the config file, default just stores in memory.
	 *
	 * @return mixed
	 */
	public function add($sKey, $sSubKey, $mValue, $bPersist = false);

	/**
	 * Gets the value or tree at this key's node.
	 *
	 * Note: if you set refresh to true, any non-persisted settings will be lost.
	 *
	 * @param string  $sKey     Config key name.
	 * @param boolean $bRefresh The counterpart to persist, this forces a re-read from the backing store.
	 *
	 * @return string|object
	 */
	public function get($sKey, $bRefresh = false);

	/**
	 * Sets the value or entire sub-tree at this key node.
	 *
	 * @param string $sKey     Config key name.
	 * @param mixed  $mValue
	 * @param bool   $bPersist Whether to write to the config file, default just stores in memory.
	 */
	public function set($sKey, $mValue, $bPersist = false);
}

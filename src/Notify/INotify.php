<?php
/**
 * @author: scipilot
 * @since : 5/12/2014
 */

namespace Scipilot\Pulse\Notify;

use Scipilot\Pulse\Pulse\Status;

interface INotify {
	/**
	 * Message is a monitoring message.
	 */
	const TYPE_MONITOR = 1;
	/**
	 * Message is a system error message or warning (self checking).
	 */
	const TYPE_INTERNAL = 2;

	/**
	 * Status messages, all is well, or has returned from alert to OK. Go back to Facebook.
	 */
	const MONITOR_STATUS = Status::OK;
	/**
	 * Something being monitored has triggered an alert. Service is disrupted or failing. Finish your coffee and check it out.
	 */
	const MONITOR_ALERT = Status::ALERT;
	/**
	 * Something being monitored has triggered an alarm. Service is down. All hands on deck Granger!
	 */
	const MONITOR_ALARM = Status::ALARM;

	/**
	 * Debug only, there won't be any in production...
	 */
	const INTERNAL_DEBUG = 1;
	/**
	 * Internal warning, perhaps something is not configured right, or an intermittent issue. Worth checking out.
	 */
	const INTERNAL_WARNING = 2;
	/**
	 * Internal error, probably continued monitoring, but you really need to diagnose them urgently.
	 */
	const INTERNAL_ERROR = 3;
	/**
	 * Internal fatal error, all monitoring will have stopped. This is the worst possible scenario.
	 */
	const INTERNAL_FATAL = 4;

	/**
	 * @todo pulse id isn't used currently. I can't decide between notify being just a super simple text-message service, or being super helpful and attaching the pulse information for you?
	 *
	 * @param int    $iType    TYPE_MONITOR | TYPE_ERROR
	 * @param int    $iLevel   [ MONITOR_STATUS | MONITOR_ALERT ] | [ INTERNAL_DEBUG | INTERNAL_WARNING | INTERNAL_ERROR | INTERNAL_FATAL ]
	 * @param int    $iPulseId Unique Pulse ID for a Service/Application.
	 * @param string $sMessage Optional; short message (status may not need one, others should)
	 *
	 * @return bool    success
	 */
	public function send($iType, $iLevel, $iPulseId, $sMessage = '');
}

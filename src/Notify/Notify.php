<?php
/**
 * Base class for all Notifiers.
 *
 * @author: scipilot
 * @since: 6/12/2014
 */
namespace Scipilot\Pulse\Notify;

use Scipilot\Pulse\App\Container;

abstract class Notify implements INotify {

	/**
	 * @var Container
	 */
	protected $app;

	/**
	 * @param Container $appContainer 	IoC
	 */
	function __construct(Container $appContainer){
		$this->app = $appContainer;
	}

}

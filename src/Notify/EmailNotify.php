<?php
/**
 * A basic email notification implementation - see config:
 *
 * notify.EmailNotify.default.email.to
 * notify.EmailNotify.default.email.subject
 * notify.EmailNotify.default.email.body - with placeholders: _TYPE_, _LEVEL_, _MESSAGE_
 * notify.EmailNotify.<TYPE>.<LEVEL>.email.to
 * notify.EmailNotify.<TYPE>.<LEVEL>.email.subject
 * notify.EmailNotify.<TYPE>.<LEVEL>.email.body - with placeholders: _TYPE_, _LEVEL_, _MESSAGE_
 *
 * @author: scipilot
 * @since: 6/12/2014
 */
namespace Scipilot\Pulse\Notify;

class EmailNotify extends Notify {

	const CONFIG_ROOT = 'notify.EmailNotify';

	public function send($iType, $iLevel, $iPulseId, $sMessage = '') {

		$to = $this->getConfig($iType, $iLevel, 'email.to');
		$subject = $this->langPlaceholders($this->getConfig($iType, $iLevel, 'email.subject'), $iType, $iLevel, '');
		$body = $this->langPlaceholders($this->getConfig($iType, $iLevel, 'email.body'), $iType, $iLevel, $sMessage);

		if (!($bResult = mail($to, $subject, $body))) {
			$this->app->log->write('ERROR: failed to send mail to:' . $to);
		}

		return $bResult;
	}

	/**
	 * Gets config value from specific case or default fallback.
	 *
	 * @param int $iType
	 * @param int $iLevel
	 * @param string $sKey
	 * @return string
	 */
	protected function getConfig($iType, $iLevel, $sKey) {
		$val = $this->app->config->get(self::CONFIG_ROOT . '.' . $iType . '.' . $iLevel . '.' . $sKey);
		if (empty($val)) $val = $this->app->config->get(self::CONFIG_ROOT . '.default.' . $sKey);
		return $val;
	}

	private function langPlaceholders($template, $iType, $iLevel, $sMessage){
		return str_replace(
			array('_TYPE_', '_LEVEL_', '_MESSAGE_'),
			array(
				$this->langType($iType),
				$this->langLevel($iType, $iLevel),
				$sMessage),
			$template);
	}

	// todo quick language conversion - do this nicer!
	private function langType($iType){
		return $this->lang('TYPE_'.$iType);
	}
	private function langLevel($iType, $iLevel){
		return ($iType == self::TYPE_MONITOR) ? $this->lang('MONITOR_'.$iLevel) : $this->lang('INTERNAL_'.$iLevel);
	}

	// TODO! QUICK LANG FUNC - TODO MAKE A LANG INJECTOR CLASS
	private function lang($sLangId) {

		$lang = array(
			'TYPE_1' 		=> "MONITOR",
			'TYPE_2'   		=> "INTERNAL",
			'MONITOR_1' 	=> "OK",
			'MONITOR_2' 	=> "ALERT",
			'MONITOR_3' 	=> "ALARM",
			'INTERNAL_1'  	=> "DEBUG",
			'INTERNAL_2'	=> "WARNING",
			'INTERNAL_3'  	=> "ERROR",
			'INTERNAL_4'  	=> "FATAL"
		);
		return $lang[$sLangId];
	}
}

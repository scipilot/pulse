<?php
/**
 * Represents the alarm status the Pulses can be in.
 * @author: scipilot
 * @since: 6/12/2014
 */
namespace Scipilot\Pulse\Pulse;

class Status {
	/**
	 * Status "OK": all is well.
	 */
	const OK = 1;
	/**
	 * In "ALERT" state: warns that the service is disrupted or failing, but helps buffer from "flapping".
	 */
	const ALERT = 2;
	/**
	 * In "ALARM" state: Service is down.
	 */
	const ALARM = 3;

}

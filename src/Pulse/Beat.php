<?php
/**
 * Small DTO for the heart-beat event.
 *
 * @author: scipilot
 * @since : 5/12/2014
 */
namespace Scipilot\Pulse\Pulse;

class Beat {

	/**
	 * When it happened
	 * @var \DateTime
	 */
	public $dtTimestamp;

	/**
	 * Unique Pulse ID for a Service/Application.
	 * @var int
	 */
	public $iPulseId;

	/**
	 * @param int	 			$iPulseId
	 * @param \DateTime $dtTimestamp [optional] Sets to NOW if omitted.
	 */
	function __construct($iPulseId, \DateTime $dtTimestamp=null) {
		$this->dtTimestamp = $dtTimestamp ? $dtTimestamp : new \DateTime();
		$this->iPulseId    = $iPulseId;
	}
}

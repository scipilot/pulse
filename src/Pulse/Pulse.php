<?php
/**
 * A Pulse is a monitoring channel representing a critical aspect of a service.
 *
 * A service may only have one pulse - just if it's still alive and operating.
 * More complex services could have multiple pulses for various aspects.
 *
 * @author: scipilot
 * @since: 5/12/2014
 */

namespace Scipilot\Pulse\Pulse;

use Scipilot\Pulse\App\Contained;
use Scipilot\Pulse\App\Container;

class Pulse extends Contained {

	/**
	 * @var int
	 */
	protected $id;

	/**
	 * @var bool
	 */
	protected $active = false;

	/**
	 * @var string
	 */
	protected $name;

	/**
	 * How many seconds to wait before assuming the service is in alert status.
	 * @var int
	 */
	protected $alertTimeout;

	/**
	 * How many seconds to wait before assuming the service is in alarm status.
	 * @var int
	 */
	protected $alarmTimeout;

	/**
	 * @var int
	 */
	public $status = Status::OK;

	/**
	 * @param Container $appContainer IoC
	 * @param int       $id           Unique Pulse ID for a Service/Application.
	 * @param string    $name					User-friendly, unique Service name for alerts and logs.
	 */
	function __construct(Container $appContainer, $id, $name){
		parent::__construct($appContainer);

		$this->id = $id;
		$this->name = $name;
	}

	function active(){
		return $this->active;
	}

	/**
	 * We are still alive.
	 */
	function beat(){
		if($this->app->storage) $this->app->storage->write(new Beat($this->id));
		else $this->app->log->write(__METHOD__.' ERROR: No storage to write to!');
	}

	function id(){
		return $this->id;
	}

	function name(){
		return $this->name;
	}

	function alertThreshold(){
		return $this->alertTimeout;
	}

	function alarmThreshold(){
		return $this->alarmTimeout;
	}

	/**
	 * Gets a config-object representation of this class.
	 * @return \stdClass (object)array('id'=>int, 'name'=>string, 'alertTimeout'=>int, 'alarmTimeout'=>int, 'status'=>int),
	 */
	public function getConfig(){
		// todo: should parent initialise this? symmetrical with set()
		$oConfig = new \stdClass();
		$oConfig->id = $this->id;
		$oConfig->name = $this->name;

		$oConfig->active = $this->active;
		$oConfig->alertTimeout = $this->alertTimeout;
		$oConfig->alarmTimeout = $this->alarmTimeout;
		$oConfig->status = $this->status;

		return $oConfig;
	}

	/**
	 * Re-sets up this class from a config-object representation
	 * @param $oConfig \stdClass (object)array('alertTimeout'=>int, 'alarmTimeout'=>int, 'status'=>int)
	 */
	public function setConfig($oConfig){
		if(!empty($oConfig->active)) $this->active = $oConfig->active;
		if(!empty($oConfig->alertTimeout)) $this->alertTimeout = $oConfig->alertTimeout;
		if(!empty($oConfig->alarmTimeout)) $this->alarmTimeout = $oConfig->alarmTimeout;
		if(!empty($oConfig->status)) $this->status = $oConfig->status;
	}
}

<?php
/**
 * Contains a simple lookup of known Pulses, for monitoring and reporting purposes.
 *
 * It essentially persists the core state of each Pulse (without persisting the entire object).
 * However you do need to keep the registry up to date with any changes to the Pulses.
 *
 * @todo can we auto-persist the Pulses? e.g. use references? and/or setter hooks? if the pulses new about the containing registry, they could call registry->save when a setter was called?
 *
 * @author: scipilot
 * @since: 5/12/2014
 */
namespace Scipilot\Pulse\Pulse;

use Scipilot\Pulse\App\Contained;
use Scipilot\Pulse\App\Container;

class PulseRegistry extends Contained {

	function __construct(Container $appContainer){
		parent::__construct($appContainer);
	}

	/**
	 * Gets an array of all active Pulse Ids.
	 */
	public function listPulses(){
		$a = array();
		$b = array();

		$pulseConfigs = $this->app->config->get('pulses');
		if(!empty($pulseConfigs)) $a = array_keys((array)$pulseConfigs);

		// stip off the _ prefixes (which prevent index collision in php's weird dual-purpose arrays).
		foreach($a as $pid){
			$b[] = substr($pid, 1);
		}

		return $b;
	}

	/**
	 * Registers a Pulse in the config, to be monitored.
	 *
	 * @param Pulse $pulse
	 */
	public function add(Pulse $pulse){
		$pulseConfig = $pulse->getConfig();

		$this->app->config->add('pulses', '_'.$pulse->id(), $pulseConfig, true);
	}
	/**
	 * Instantiates a Pulse from its configuration.
	 *
	 * @param int $iPulseId
	 * @return Pulse|null
	 */
	public function get($iPulseId){
		/** @var Pulse $pulse */
		$pulse = null;

		$conf = $this->app->config->get('pulses');
		if($conf){
			$key = '_'.$iPulseId;
			$pulseConf = $conf->$key;
			if($pulseConf){
				$pulse = new Pulse($this->app, $pulseConf->id, $pulseConf->name);
				$pulse->setConfig($pulseConf);
			}
		}
		return $pulse;
	}

	/**
	 * Removes a Pulse from the registry - to stop monitoring this service.
	 *
	 * @param int $iPulseID
	 */
	public function remove($iPulseID){
		$this->app->config->add('pulses', '_'.$iPulseID, null, true); //write an unset?
	}
}

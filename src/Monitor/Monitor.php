<?php
/**
 * The second half of the system is the monitor, which frequently checks the storage for recent pulses.
 *
 * This should be run from a top-level service script, itself monitored by supervisord. AlwaysUp or similar.
 *
 * Either just call run() if you trust this daemon. Or call scan() repeatedly on your own schedule.
 *
 * Run() Requires 'monitor.run.loop' config:
 *
 * 		'monitor.run.interval' : seconds, scan poll time
 * 		'monitor.run.lifespan' : seconds, suicide time, or 0 for infinite lives.
 *
 * Monitor Algorithm:
 *
 *  - Get the list of all Scopes (if any)
 *  	- Use each Scope to listen to each connected active Pulse, and record a Beat
 *
 * 	- Get the list of all Pulses to take from the config.
 * 	- Load the latest Beat from the storage for each active Pulse.
 * 	- Check if the timestamp is older than the ALARM threshold, OR no heartbeat:
 * 		- If it is, mark Pulse as status ALARM
 * 		- Send notification.
 * 	- else Check if the timestamp is older than the ALERT threshold:
 * 		- If it is, mark Pulse as status ALERT
 * 		- Send notification.
 *  - else If it's OK and the current status is not OK
 *		- Reset the Pulse as status OK
 * 		- Send a STATUS Notification (saying we're good again)
 *
 * @author: scipilot
 * @since: 6/12/2014
 */
namespace Scipilot\Pulse\Monitor;

use Scipilot\Pulse\App\Container;
use Scipilot\Pulse\Log\ILog;
use Scipilot\Pulse\Notify\EmailNotify;
use Scipilot\Pulse\Notify\INotify;
use Scipilot\Pulse\Pulse\Pulse;
use Scipilot\Pulse\Pulse\Beat;
use Scipilot\Pulse\Pulse\PulseRegistry;
use Scipilot\Pulse\Pulse\Status;
use Scipilot\Pulse\Scope\Scope;
use Scipilot\Pulse\Scope\ScopeRegistry;

class Monitor {

	/**
	 * @var Container
	 */
	protected $app;

	/**
	 * @var PulseRegistry
	 */
	protected $pulseRegistry;
	/**
	 * @var ScopeRegistry
	 */
	protected $scopeRegistry;

	/**
	 * @param Container $appContainer 	IoC
	 */
	function __construct(Container $appContainer){
		$this->app = $appContainer;

		$this->pulseRegistry = new PulseRegistry($this->app);

		// TODO PCNTL SHUTDOWN CONTROL
		//$this->setupShutdown();
	}

	private function setupShutdown(){
		// trap shutdown to close comms and kill the other processes.
		register_shutdown_function(array($this, 'shutdown'));
		// TODO IS THERE A NICER WAY TO SIGNAL TERMINATION E.G. WRITING TO A FILE? OR VIA AMQP?
		// pcntl can trap termination signals, else the shutdown won't be detected!
		if (function_exists('pcntl_signal')) {
			$this->app->log->write("PCNTL installed. Setting up termination trap...", ILog::LOG_LEVEL_DEBUG);
			pcntl_signal(SIGINT, array($this, 'quit'));
			pcntl_signal(SIGTERM, array($this, 'quit'));
		}
		else {
			$this->app->log->write('Termination signals cannot be trapped due to missing pcntl library, so a clean shutdown is not possible.'
				. 'You must a restart mechanism (e.g. supervisord, AlwaysUp),', ILog::LOG_LEVEL_WARNING);
		}
	}

	private function shutdown() {
		// todo: fork/respawn?

		$this->app->log->write("\n".' !!!!!!!!!! SHUTDOWN !!!!!!!!!!!! ' . __METHOD__, ILog::LOG_LEVEL_ERROR);

		// Try to send a notification
		if($this->app->notify->send(INotify::TYPE_INTERNAL, INotify::INTERNAL_FATAL, 'ALL', 'MONITOR SHUTDOWN - TRAPPED FATAL ERROR')){
			$this->app->log->write("\n" . __METHOD__. ' Failed to send notification. ', ILog::LOG_LEVEL_ERROR);
		}
	}


	/**
	 * The main run-loop, (if you want this daemon to do it, else just call scan() from a top-level script).
	 * Will block until killed or the configured lifespan elapses.
	 */
	public function run(){
		$birth = time();

		do {
			$this->scan();

			// reread the config, so we can be reconfigured on the fly
			$interval = $this->app->config->get('monitor.run.interval', true);
			$lifespan = $this->app->config->get('monitor.run.lifespan', true);

			sleep($interval);
			$age = time() - $birth;
		}
		while($lifespan==0 || $age < $lifespan);
	}

	/**
	 * Before testing all the heartbeats, we see if we are required to use any "remote sensing" scopes.
	 * If none are configured, then we assume all Beats are registered directly by the client services using Pulse->beat.
	 */
	protected function scanScopes(){
		/** @var Scope $s */
		if(empty($this->scopeRegistry)) $this->scopeRegistry = new ScopeRegistry($this->app);

		//  - Get the list of all Scopes (if any)
		$scopeIds = $this->scopeRegistry->listScopes();

		//  	- Use each Scope to listen to each connected Pulse, and record a Beat
		foreach($scopeIds as $id){
			$s = $this->scopeRegistry->get($id);

			// Check pulse is active, else don't bother!
			$p = $this->pulseRegistry->get($s->getPulseId());
			if($p->active()){

				$s->Listen();
			}
		}
	}

	// testing tunnel! do not use.
	public function testScanScopes(){
		$this->scanScopes();
	}

	/**
	 * Scans all Scopes and then all Beats.
	 */
	public function scan(){
		$this->scanScopes();
		$this->scanBeats();
	}

	/**
	 * Scans all the heartbeats. Call this every X seconds, or call run()
	 */
	public function scanBeats(){
		/** @var Pulse $p */
		/** @var Beat $beat */

		// 	- Get the list of all Pulses to take from the config.
		$pulseIds = $this->pulseRegistry->listPulses();

		// 	- Load the latest Beat from the storage for each active Pulse.
		foreach($pulseIds as $id){
			$p = $this->pulseRegistry->get($id);
			if($p->active()){
				$beat = $this->app->storage->read($id);
				if($beat){
					$s = time() - $beat->dtTimestamp->getTimestamp();
					//see note about pulse id in inotify...
					$sMessage = $p->name()."\n".' Last heartbeat was '.$s.' seconds ago.';
					$this->app->log->write(time().' - '.$beat->dtTimestamp->getTimestamp(), ILog::LOG_LEVEL_DEBUG);
				}
				else {
					$s = -1;
					$sMessage = $p->name()."\n".' No hearbeat found!';
				}
				$this->app->log->write('Monitor checking '.$sMessage, ILog::LOG_LEVEL_DEBUG);

				// 	- Check if the timestamp is older than the ALARM threshold, OR no heartbeat:
				// 		- If it is, mark Pulse as status ALARM
				// 		- Send notification.
				if($s === -1 || $s >= $p->alarmThreshold()){
					$this->app->log->write('Monitor ALARM: '.$sMessage);

					// Notify on change
					if($p->status != Status::ALARM){
						$p->status = Status::ALARM;
						$this->pulseRegistry->add($p);// this is annoying! can the pulse self-persist?

						$this->app->notify->send(
							EmailNotify::TYPE_MONITOR,
							INotify::MONITOR_ALARM, $p->id(),
							$sMessage);
					}
					continue;
				}

				// 	- else Check if the timestamp is older than the ALERT threshold:
				// 		- If it is, mark Pulse as status ALERT
				// 		- Send notification.
				if($s >= $p->alertThreshold()){
					$this->app->log->write('Monitor ALERT: '.$sMessage);

					// Notify on change
					if($p->status != Status::ALERT){
						$p->status = Status::ALERT;
						$this->pulseRegistry->add($p);// this is annoying! can the pulse self-persist?

						$this->app->notify->send(
							INotify::TYPE_MONITOR,
							INotify::MONITOR_ALERT, $p->id(),
							$sMessage);
					}
					continue;
				}

				//  - else If it's OK and the current status is not OK
				//		- Reset the Pulse as status OK
				// 		- Send a STATUS Notification (saying we're good again)
				if($p->status != Status::OK){
					$this->app->log->write('Monitor OK: '.$sMessage);

					$p->status = Status::OK;
					$this->pulseRegistry->add($p);// this is annoying! can the pulse self-persist?

					$this->app->notify->send(
						EmailNotify::TYPE_MONITOR,
						INotify::MONITOR_STATUS, $p->id(),
						$sMessage.' Service has resumed. ');
					continue;
				}
				else $this->app->log->write('Monitor still OK...', ILog::LOG_LEVEL_DEBUG);
			}
		}
	}
}

<?php
/**
 * @author: scipilot
 * @since : 5/12/2014
 */

namespace Scipilot\Pulse\Log;

interface ILog {
	const LOG_LEVEL_ERROR   = 1;
	const LOG_LEVEL_WARNING = 2;
	const LOG_LEVEL_INFO    = 3;
	const LOG_LEVEL_DEBUG   = 4;

	/**
	 * Sets the max level of logging you want to happen.
	 * @param $iVerbosity self::LOG_LEVEL_INFO etc.
	 */
	public function setVerbosity($iVerbosity);

	/**
	 * Log will be ignored if too verbose for current setting.
	 *
	 * @param string $sMessage
	 * @param int $iVerbosity Level of this message (self::LOG_LEVEL_INFO etc.)
	 */
	public function write($sMessage, $iVerbosity=self::LOG_LEVEL_INFO);

}

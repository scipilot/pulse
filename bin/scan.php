<?php
/**
 * Run this REGULARLY to monitor your services.
 * (Monitor Option B)
 *
 * Those services should be calling Pulse->beat() regularly.
 */
require_once __DIR__.'/../../../../vendor/autoload.php';
use Scipilot\Pulse\App\DefaultContainer;
use Scipilot\Pulse\Monitor\Monitor;

$app = new DefaultContainer();

$monitor = new Monitor($app);
$monitor->scan();

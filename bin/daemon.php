<?php
/**
 * Run this in its own process to monitor your services.
 * (Monitor Option A)
 *
 * Those services should be calling Pulse->beat() regularly.
 */
require_once __DIR__.'/../../../../vendor/autoload.php';
use Scipilot\Pulse\App\DefaultContainer;
use Scipilot\Pulse\Monitor\Monitor;

$app = new DefaultContainer();
//$app->log->setVerbosity(\Scipilot\Pulse\Log\ILog::LOG_LEVEL_DEBUG);// uncomment for debug

$monitor = new Monitor($app);
$monitor->run();

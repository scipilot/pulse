<?php
/**
 * Example dodgy service, which will trigger random alerts.
 *
 * @author: scipilot
 * @since: 6/12/2014
 */

require_once __DIR__.'/../vendor/autoload.php';
use Scipilot\Pulse\App\DefaultContainer;
use Scipilot\Pulse\Log\ILog;
use Scipilot\Pulse\Pulse\Pulse;
use Scipilot\Pulse\Pulse\PulseRegistry;

// This service pulse should already be configured, and the monitor should be running.
// see `/storage/config-sample.json` (copy this into config.json to use it)
// Your service/application will have it's own way of doing things, but here's a simple binding:
const MyPulseId = 101;

$app = new DefaultContainer();
$app->log->setVerbosity(ILog::LOG_LEVEL_DEBUG);
$registry = new PulseRegistry($app);
/** @var Pulse $pulse */
$pulse = $registry->get(MyPulseId);

$app->log->write(
	'Example random service starting up... Pulse name:'.$pulse->name()
	.'Notifications will go to:'. $app->config->get('notify.EmailNotify.default.email.to')
);

do {
	sleep(rand(1,10));
	$pulse->beat();
}
while(true);
